import torch
from flask import Flask
from flask_restx import Resource, Api
from flask_cors import CORS
import tensorflow as tf
import tensorflow_hub as hub
import numpy as np
from scipy.io import wavfile
import scipy.signal
from flask.helpers import flash
from werkzeug.utils import secure_filename
from werkzeug.datastructures import FileStorage
import os
from utilities import *

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024
ALLOWED_EXTENSIONS = {'wav'}
CORS(app)
api = Api(app = app,
    version = "1.1.1",
    title = "Angelica Sound AI API Documentation",
    description = "Audio is processed and classified here",
    doc = "/docs/"
)
upload_parser = api.parser()
upload_parser.add_argument('file', location='files',
                           type=FileStorage, required=True)

model_classifier = hub.load("classifier/")
kdrt_list = [6,9,10,11,19,20,21,22,352,421,422,428,430,434,435,437,460,461,462,463,464,473,474,478,479]
silence_index = 494
torch.hub.set_dir('./speech/')
device = torch.device('cpu')
model_tts,decoder, utils = torch.hub.load(repo_or_dir='snakers4/silero-models',
                                model='silero_stt',
                                language='en', # also available 'de', 'es'
                                device=device)
(read_batch, split_into_batches,
read_audio, prepare_model_input) = utils

keywords = open("keywords.txt", "r")
keywords_split = keywords.read().splitlines()
keywords.close()

def ensure_sample_rate(original_sample_rate, waveform,
                       desired_sample_rate=16000):
  """Resample waveform if required."""
  if original_sample_rate != desired_sample_rate:
    desired_length = int(round(float(len(waveform)) /
                               original_sample_rate * desired_sample_rate))
    waveform = scipy.signal.resample(waveform, desired_length)
  return desired_sample_rate, waveform




predict_namespace = api.namespace('audio', description='Determine if audio has signs of KDRT')

# model_audio = api.schema_model('Send audio data', {
#         'required' : ['data'],
#         'properties' : {
#             'data' : [],
#         },
#         'type' : 'object'
#     }
# )

# model_audio = api.model("Send audio data",
#     {"data" : fields.String(required = True,
#         description = "Audio file bits in Base64 format (wav)",
#         help = "data must be .wav encoded in Base64 format")
#     }
# )

@predict_namespace.route("/predict")
class Predict(Resource):
    @api.doc(responses={ 200: 'OK', 400: 'Invalid Input', 500: 'Mapping Key Error' })
    # @api.expect(model_audio)
    @api.expect(upload_parser)
    def post(self):
        args = upload_parser.parse_args()
        file = args['file']
        file_name = secure_filename(file.filename)
        basepath = os.path.dirname(__file__)
        file.save(os.path.join(
            basepath,file_name
        ))

        # ### Convert base64 into .wav file
        # base64_audio = request.json['data']
        # wav_file = open("audio.wav","wb")
        # decode_string = base64.b64decode(base64_audio)
        # wav_file.write(decode_string)
        
        ### Speech to Text
        batches = split_into_batches([file_name], batch_size=10)
        input = prepare_model_input(read_batch(batches[0]),
                                    device=device)
        output = model_tts(input)
        
        def detect_kdrt():
            for example in output:
                split_sentence = decoder(example.cpu()).split()
                for i in range(len(split_sentence)):
                    if(split_sentence[i] in keywords_split):
                        return True
            return False
        is_kdrt_tts = detect_kdrt() 
        
        ### Audio Classifier
        # Get wave data
        wav_file_name = file_name
        sample_rate, wav_data = wavfile.read(wav_file_name, 'rb')
        sample_rate, wav_data = ensure_sample_rate(sample_rate, wav_data)
        wav_data = np.array(wav_data)
        waveform = wav_data / tf.int16.max

        # Get prediction
        scores, embeddings, spectrogram = model_classifier(waveform)
        scores_np = scores.numpy()

        # Determine presence of KDRT
        kdrt = 0
        silence = 0
        frame_count = len(scores_np)
        threshold = frame_count//2
        for i in range(0, threshold, 2):
            highest_score = scores_np[i].argmax()

            if highest_score == silence_index:
                silence += 1
            elif binary_search(kdrt_list, highest_score) != -1:
                kdrt += 1
            
            if kdrt > threshold:
                break

        is_kdrt_classifier = False
        not_kdrt_count = threshold - silence - kdrt
        if(kdrt > not_kdrt_count):
            is_kdrt_classifier = True

        # Judgement
        is_kdrt = is_kdrt_tts or is_kdrt_classifier
        os.remove("{file_name}".format(file_name=file_name))
        return {
            'status' : 'Success',
            'statusCode' : 200,
            'is_kdrt' : is_kdrt
        }

if __name__ == '__main__':
    # Start flask server
    app.run(host='0.0.0.0')
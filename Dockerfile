FROM python:3.8

WORKDIR /kdrt-backend-audio

RUN pip install torch==1.10.1

RUN pip install flask==1.1.0

RUN pip install flask-restx==0.5.1

RUN pip install flask-cors==3.0.10

RUN pip install omegaconf==2.1.1

RUN pip install torchaudio==0.10.1

RUN pip install torchvision

RUN pip install numpy==1.18.5

RUN pip install tensorflow==2.7.0

RUN pip install tensorflow-hub==0.12.0

RUN pip install scipy==1.4.1

RUN pip install werkzeug==2.0.2


EXPOSE 5000

COPY . .

CMD [ "python3", "-u", "-m", "flask", "run", "--host=0.0.0.0"]